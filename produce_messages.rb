require 'poseidon'
require 'msgpack'

TOPIC = "test_scala"

producer = Poseidon::Producer.new(["localhost:9092"], "my_test_producer")

messages = []

ids = Array.new(100) { |i| i }
events = %w{request fill click impression}

events.each do |event|
  ids.each do |id|
    messages << Poseidon::MessageToSend.new(TOPIC, { event: event, ad_id: id }.to_msgpack)
    if messages.size > 100
      producer.send_messages messages
      messages = []
    end
  end
end

producer.send_messages(messages)
