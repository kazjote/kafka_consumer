package fyber.trial_consumer

import java.util.Properties
import kafka.consumer.ConsumerConfig
import kafka.consumer.Whitelist
import kafka.serializer.DefaultDecoder
import rx.lang.scala.Observable
import kafka.message.MessageAndMetadata
import rx.lang.scala.subjects.ReplaySubject
import kafka.message.MessageAndMetadata
import rx.lang.scala.Subscription
import scala.concurrent.duration._
import scala.concurrent._
import ExecutionContext.Implicits.global
import rx.lang.scala.Scheduler
import scala.collection.mutable.HashMap
import org.msgpack.MessagePack
import scala.collection.JavaConversions._
import rx.lang.scala.Scheduler
import rx.lang.scala.schedulers.IOScheduler
import rx.schedulers.Schedulers
import rx.lang.scala.Scheduler
import rx.lang.scala.schedulers.NewThreadScheduler

object Consumer {
  type KafkaMessage = MessageAndMetadata[Array[Byte], Array[Byte]]
}

class Consumer {
  val properities: Properties = new Properties

  properities.put("group.id", "presentation_consumer")
  properities.put("zookeeper.connect", "localhost:2181")
  properities.put("auto.offset.reset", "smallest")
  properities.put("auto.commit.enable", "false")

  val config = new ConsumerConfig(properities)
  val connector = kafka.consumer.Consumer.create(config)

  val filterSpec = new Whitelist("test_scala")

  val stream = connector.createMessageStreamsByFilter(filterSpec, 1, new DefaultDecoder(), new DefaultDecoder()).head

  def decoder(messageAndMetadata: Consumer.KafkaMessage) = {
    val unpackedMessage: HashMap[String, Any] = new HashMap

      val unpacked = MessagePack.unpack(messageAndMetadata.message)
      for (entry <- unpacked.asMapValue.entrySet) {
        val value: Any = entry.getValue match {
          case v if v.isBooleanValue => v.asBooleanValue
          case v if v.isIntegerValue => v.asIntegerValue
          case v if v.isFloatValue => v.asFloatValue
          case v if v.isRawValue => v.asRawValue.getString
          case v => None
        }
        unpackedMessage.put(entry.getKey.asRawValue.getString, value) 
      }

    unpackedMessage
  }

  def observable = Observable[Consumer.KafkaMessage](observer => {
    for (messageAndMetadata <- stream) {
      observer.onNext(messageAndMetadata)
    }
  }).observeOn(IOScheduler()).map(m => decoder(m))
}
