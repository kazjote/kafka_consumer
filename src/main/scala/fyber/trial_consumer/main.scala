package fyber.trial_consumer

import scala.concurrent.duration._

object Main {

  def main(args: Array[String]): Unit = {
    val consumer = new Consumer
    
    consumer.observable.sample(1.second).subscribe(m => println(m))
  }

}
