name := "Kafka trial consumer"

version := "0.0.1"

scalaVersion := "2.10.4"

resolvers += "spray repo" at "http://repo.spray.io"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "spray repo" at "http://repo.spray.io"

libraryDependencies += "org.apache.kafka" %% "kafka" %"0.8.1" exclude("log4j", "log4j") exclude("org.slf4j","slf4j-log4j12")

libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % "2.3.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "2.0"

libraryDependencies += "commons-io" % "commons-io" % "2.4"

libraryDependencies += "org.slf4j" % "log4j-over-slf4j" % "1.6.6"

libraryDependencies += "com.netflix.rxjava" % "rxjava-scala" % "0.20.6"

libraryDependencies += "org.msgpack" % "msgpack" % "0.6.11"
